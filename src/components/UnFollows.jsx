import { useEffect, useState } from 'react';
import './UnFollows.css';

function Following({ tweets, unFollowUser, currentUser, setFilteredTweets, setFilterActive }) {
  const [usersFollowed, setUsersFollowed] = useState([]);

  useEffect(() => {
    if (tweets.length > 0) {
      const usersFollowed = tweets.filter(item => item.user !== currentUser);
      setUsersFollowed(usersFollowed.map(tweet => ({
        user: tweet.user,
        comments_count: tweet.comments_count,
        content: tweet.content,
        created_on: tweet.created_on,
        favorites_count: tweet.favorites_count,
        followed: tweet.followed,
        id: tweet.id,
        retweets_count: tweet.retweets_count,
      })));
    } else {
      setUsersFollowed([])
    }
  }, [tweets])

  const filterByUser = (user) => {
    console.log(user)
    const tweetsFiltered = tweets.filter(item => item.user === user);
    console.log(tweetsFiltered)
    setFilterActive(true);
    setFilteredTweets(tweetsFiltered);
  }

  return (
    <div>
      {usersFollowed && usersFollowed.length !== 0 ? (
        <>
          <p>Following</p>
          <div className='followContainer'>
            {usersFollowed.map((users, id) => (
              <ul key={id}>
                <a className='user' onClick={() => filterByUser(users.user)}>@{users.user}</a> <button className='follow-button' onClick={() => unFollowUser(users)}>Unfollow</button>
              </ul>
            ))}
          </div>
        </>
      ) : (
        <></>
      )}
    </div>
  )

}

export default Following;