import { useState, useEffect } from 'react';
import ComposeForm from './ComposeForm';
import Timeline from './Timeline';
import Following from './UnFollows';
import ShouldFollow from './ShouldFollow';
import { FaTwitter } from 'react-icons/fa';
import { nanoid } from 'nanoid';
import './App.css';

import initialTweets from '../tweets.json';

const CURRENT_USER = 'jrguez';

function App() {
  const [tweets, setTweets] = useState([]);
  const [filteredTweets, setFilteredTweets] = useState([]);
  const [filterActive, setFilterActive] = useState(false);
  const [favorites, setFavorites] = useState([]);
  const [usersToFollow, setUsersToFollow] = useState([])

  useEffect(() => {
    setTweets(initialTweets.filter(initialTweets => initialTweets.followed === true));
    setUsersToFollow(initialTweets.filter(initialTweets => initialTweets.followed !== true))
    const notFollowed = tweets.filter(tweets => tweets.followed === false);
    console.log(notFollowed)

  }, [])

  const handlePostTweet = (content) => {
    const newTweet = {
      content,
      id: nanoid(),
      created_on: Date(Date.now()),
      user: CURRENT_USER,
      comments_count: 0,
      retweets_count: 0,
      favorites_count: 0,
      followed: false
    };
    setTweets([...tweets, newTweet]);
  };

  const handleToggleFavorite = (tweetId) => {
    const foundIndex = favorites.indexOf(tweetId);

    if (foundIndex > -1) {
      setFavorites(favorites.filter((favoriteId) => favoriteId !== tweetId));
    } else {
      setFavorites([...favorites, tweetId]);
    }
  };

  const handleUnfollowUser = (userDeleted) => {
    const unfollowUser = tweets.filter(item => item.user !== userDeleted.user);
    if (unfollowUser.length === 0) {
      setTweets([]);
    } else {
      setTweets([...unfollowUser]);
    }
    usersToFollow.push(userDeleted);
    const shouldFollow = usersToFollow;
    setUsersToFollow([...shouldFollow])
  }

  const handleFollowUser = (userAdded) => {
    const deleteUserFollowed = usersToFollow.filter(item => item.user !== userAdded.user)
    if (deleteUserFollowed.length === 0) {
      setUsersToFollow([...deleteUserFollowed]);
    } else {
      setUsersToFollow([...deleteUserFollowed]);
    }
    tweets.push(userAdded);
    const followUser = tweets;
    setTweets([...followUser])

  }

  return (
    <div className="app">
      <div className='followsContainer'>
        <Following
          tweets={tweets}
          unFollowUser={handleUnfollowUser}
          currentUser={CURRENT_USER}
          setFilteredTweets={setFilteredTweets}
          setFilterActive={setFilterActive}
        />
        <ShouldFollow
          tweets={usersToFollow}
          followUser={handleFollowUser}
          currentUser={CURRENT_USER}
        />
      </div>
      <div className="tweets-container">
        <FaTwitter onClick={() => setFilterActive(false)} className="app-logo" size="2em" />
        {filterActive ? (
          <button className="back-button" onClick={() => setFilterActive(false)}>Volver</button>
        ) : null}
        <ComposeForm onSubmit={handlePostTweet} />
        <div className="separator"></div>
        {filterActive ? (
          <Timeline
            tweets={filteredTweets}
            onRetweet={handlePostTweet}
            favorites={favorites}
            onToggleFavorite={handleToggleFavorite}
            setFilteredTweets={setFilteredTweets}
            setFilterActive={setFilterActive}
          />
        ) : (
          <Timeline
            tweets={tweets}
            onRetweet={handlePostTweet}
            favorites={favorites}
            onToggleFavorite={handleToggleFavorite}
            setFilteredTweets={setFilteredTweets}
            setFilterActive={setFilterActive}
          />
        )}
      </div>
    </div>
  );
}

export default App;
