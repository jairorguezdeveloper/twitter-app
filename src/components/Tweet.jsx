import moment from 'moment';
import PropTypes from 'prop-types';
import Avatar from './Avatar';
import './Tweet.css';

function Tweet(props) {
  const { user, createdOn, children, setFilteredTweets, setFilterActive, tweets } = props;

  const handleFilterByUser = (user) => {
    console.log(user)
    const tweetsFiltered = tweets.filter(item => item.user === user);
    console.log(tweetsFiltered)
    setFilterActive(true);
    setFilteredTweets(tweetsFiltered);
  }

  return (
    <div className="tweet">
      <Avatar name={user} />
      <div>
        <div className="tweet-header">
          <a onClick={() => handleFilterByUser(user)}><span className="tweet-user" >@{user}</span></a>·
          <span className="tweet-created-on">
            {moment(createdOn).fromNow()}
          </span>
        </div>
        <div className="tweet-content">{children}</div>
      </div>
    </div>
  );
}

Tweet.propTypes = {
  user: PropTypes.string,
  createdOn: PropTypes.string,
};

export default Tweet;
