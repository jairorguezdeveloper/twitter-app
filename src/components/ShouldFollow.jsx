import { useEffect, useState } from 'react';
import './ShouldFollow.css';

function ShouldFollow({ tweets, followUser, currentUser }) {
  const [usersShouldFollow, setUsersShouldFollowed] = useState([]);

  useEffect(() => {
    if (tweets.length > 0) {
      const usersFollowed = tweets.filter(item => item.user !== currentUser);
      setUsersShouldFollowed(usersFollowed.map(tweet => ({
        user: tweet.user,
        comments_count: tweet.comments_count,
        content: tweet.content,
        created_on: tweet.created_on,
        favorites_count: tweet.favorites_count,
        followed: tweet.followed,
        id: tweet.id,
        retweets_count: tweet.retweets_count,
      })));
    } else {
      setUsersShouldFollowed([]);
    }
  }, [tweets])

  return (
    <div>
      {usersShouldFollow && usersShouldFollow.length !== 0 ? (
        <>
          <p>Should Follow</p>
          <div className='followContainer'>
            {usersShouldFollow.map((users, id) => (
              <ul key={id}>
                @{users.user} <button className='follow-button' onClick={() => followUser(users)}>Follow</button>
              </ul>
            ))}
          </div>
        </>
      ) : (
        <></>
      )
      }
    </div>
  )

}

export default ShouldFollow;